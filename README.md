# LrDB

This is a reimplementation of LrDB with postgresql, and more importantly diesel.

## Postgresql Setup

### Install postgresql

Debian:

```
$ sudo apt install postgresql postgresql-client
```

ArchLinux:

```
$ sudo pacman -S postgresql
```

Fedora/RHEL

```
$ sudo yum install postgresql
```

### Log into the postgresql client

Set the password as follows.

```
$ sudo passwd postgres
```

Now you need to login to postgres, using the password you just entered.

```
$ su -l postgres
```

### Initial Configuration

Substitute locale with a locale installed on the system (for example `en_GB.UTF-8`)

```
[postgres]$ initdb --locale=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
```

Hopefully everything will say `... ok` at the end.

> If you are on `btrfs` then make sure to disable CoW
>
> ```
> chattr +C /var/lib/postgres/data/
> ```

Start postgresql service:

```
$ sudo systemctl enable postgresql.service --now
```

### Create a database and user.

Create the user:

I will assume the user is called `lrdb` and the password `1234`. (Obviously for production user don't do this because it is extremely insecure.)

```
[postgres]$ createuser --interactive
```

And create the database. The first `lrdb` is the database name, the second is the user.

```
[postgres]$ createdb lrdb -O lrdb
```

### Login to the database

```
[postgres]$ psql -d lrdb
```

And change the password

```
lrdb~#$ ALTER ROLE lrdb WITH PASSWORD 1234
```

## Diesel Setup

Install the diesel CLI tool:

```
$ cargo install diesel_cli --no-default-features --features postgres
```

If you haven't used the username and password above modify the .env file in the format:

> `.env`
> ```
> DATABASE_URL=postgres://username:password@localhost/diesel_demo
> ```

And now setup disel:

```
diesel setup
```
