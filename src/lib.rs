#![warn(clippy::pedantic)]
#![allow(
    clippy::missing_errors_doc,
    clippy::missing_panics_doc,
    clippy::used_underscore_binding,
    clippy::module_name_repetitions
)]
extern crate lazy_static;

pub mod database;
pub use database::Database;
pub mod error;
