use std::error::Error;
use std::fmt;

/// Server Or User Error
#[derive(Debug)]
pub enum SOUError<T: Error> {
    User(T),
    Server(String),
}

impl<T: Error> fmt::Display for SOUError<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::User(e) => fmt::Display::fmt(e, f),
            Self::Server(e) => fmt::Display::fmt(e, f),
        }
    }
}

impl<T: Error, U: Error> From<U> for SOUError<T> {
    /// Automatically create a server error.
    fn from(them: U) -> Self {
        Self::Server(them.to_string())
    }
}
