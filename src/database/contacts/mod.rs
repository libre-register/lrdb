//! # Contact
//!
//! This contains functions for contacts.
use super::Database;
use contack::sql::SqlContact;
use contack::Contact;
use std::convert::TryInto;
use std::error::Error;

impl Database {
    /// Deletes all contact information associated with a contact,
    /// so that it can be reinserted.
    pub async fn delete_all_ci(
        &self,
        contact: &Contact,
    ) -> Result<(), sqlx::Error> {
        sqlx::query!(
            "DELETE FROM contacts_contact_information
            WHERE uid = $1;",
            contact.uid
        )
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    /// Inserts a list of contact information into a table, so that it can
    /// be used. This will first delete all prior contact information from
    /// a contact and then insert the new one. This is called when inserting
    /// a contact so you should probably just insert a whole contact instead.
    pub async fn insert_ci(
        &self,
        contact: &Contact,
    ) -> Result<(), sqlx::Error> {
        self.delete_all_ci(contact).await?;

        // Loop through all the contact information
        for ci in &contact.contact_information {
            sqlx::query(
                "
                INSERT INTO contacts_contact_information
                (pid, pref, value, typ, platform, uid)
                VALUES ($1, $2, $3, $4, $5, $6);",
            )
            .bind(&ci.pid)
            .bind(i32::from(ci.pref))
            .bind(&ci.value)
            .bind(&ci.typ)
            .bind(&ci.platform)
            .bind(&contact.uid)
            .execute(&self.pool)
            .await?;
        }

        Ok(())
    }

    /// Inserts a contact
    pub async fn insert_contact(
        &self,
        contact: &mut Contact,
    ) -> Result<(), Box<dyn Error>> {
        // Inserts the contact information.
        self.insert_ci(contact).await?;

        let contact: SqlContact = std::mem::take(contact).try_into()?;
        println!("{:#?}", contact.bday);

        // Inserts the contact.
        //
        // Dont ya just love sql
        sqlx::query!(
            "
            INSERT INTO contacts
            (
                uid,
                name_given,
                name_additional,
                name_family,
                name_prefixes,
                name_suffixes,
                nickname,
                anniversary,
                bday,
                photo_uri,
                photo_bin,
                photo_mime,
                title,
                role,
                org_org,
                org_unit,
                org_office,
                logo_uri,
                logo_bin,
                logo_mime,
                home_address_street,
                home_address_locality,
                home_address_region,
                home_address_code,
                home_address_country,
                home_address_geo_longitude,
                home_address_geo_latitude,
                work_address_street,
                work_address_locality,
                work_address_region,
                work_address_code,
                work_address_country,
                work_address_geo_longitude,
                work_address_geo_latitude
            )
            VALUES (
                $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,
                $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26,
                $27, $28, $29, $30, $31, $32, $33, $34
            );
            ",
            contact.uid,
            contact.name_given,
            contact.name_additional,
            contact.name_family,
            contact.name_prefixes,
            contact.name_suffixes,
            contact.nickname,
            contact.anniversary,
            contact.bday,
            contact.photo_uri,
            contact.photo_bin,
            contact.photo_mime,
            contact.title,
            contact.role,
            contact.org_org,
            contact.org_unit,
            contact.org_office,
            contact.logo_uri,
            contact.logo_bin,
            contact.logo_mime,
            contact.home_address_street,
            contact.home_address_locality,
            contact.home_address_region,
            contact.home_address_code,
            contact.home_address_country,
            contact.home_address_geo_longitude,
            contact.home_address_geo_latitude,
            contact.work_address_street,
            contact.work_address_locality,
            contact.work_address_region,
            contact.work_address_code,
            contact.work_address_country,
            contact.work_address_geo_longitude,
            contact.work_address_geo_latitude,
        )
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}
