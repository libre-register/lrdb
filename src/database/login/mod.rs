//! # Login
//!
//! This contains functions to do with logging in, managing users, and
//! changing permissions.
use super::Database;
use std::error::Error;
pub mod error;
use crate::error::SOUError;
pub use error::*;

struct LoginSqlxRet {
    username: String,
    password: String,
    permissions: serde_json::Value,
}

impl Database {
    /// Gets a login from a database.
    ///
    /// May return
    /// [LoginError::IncorrectUsername](LoginError::IncorrectUsername)
    pub async fn get_login(
        &self,
        username: &str,
    ) -> Result<lrau::User, SOUError<LoginError>> {
        // Query the row
        let row = sqlx::query_as!(
            LoginSqlxRet,
            "SELECT username,password,permissions \
                FROM LOGIN_USERS \
                WHERE username = $1 \
                LIMIT 1;",
            username
        )
        .fetch_one(&self.pool)
        .await
        .map_err(|x| match x {
            sqlx::Error::RowNotFound => {
                SOUError::User(LoginError::IncorrectUsername)
            }
            _ => SOUError::Server(x.to_string()),
        })?;

        // Turn it into a login struct.
        Ok(lrau::User {
            username: row.username,
            password: row.password,
            permissions: serde_json::from_value(row.permissions)?,
            expire: None,
            logged_in: None,
        })
    }

    /// Attempts to log into a user. Both the username and password are in
    /// plain text, for your benifit.
    pub async fn login(
        &self,
        username: &str,
        password: &str,
    ) -> Result<lrau::User, SOUError<LoginError>> {
        let mut login = self.get_login(&username).await?;

        // Tries to log in.
        if login
            .log_in(password, self.config.login_timeout)
            .unwrap_or(false)
        {
            Ok(login)
        } else {
            Err(SOUError::User(LoginError::IncorrectPassword))
        }
    }

    /// Inserts a new login user to the database.
    ///
    /// # Panics
    ///
    /// Panics at compile time if the sql is not valid. You may have to
    /// setup a postgres database to compile certain function.
    pub async fn insert_login_user(
        &self,
        user: &lrau::User,
    ) -> Result<(), Box<dyn Error>> {
        sqlx::query!(
            "
            INSERT INTO LOGIN_USERS
            VALUES ($1, $2, $3)
            ",
            user.username,
            user.password,
            serde_json::to_value(&user.permissions)?,
        )
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}
