use std::error::Error;
use std::fmt;

/// This is the error type when failing to log in.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum LoginError {
    /// This indicates that the user's username does not exist in the
    /// database.
    IncorrectUsername,
    /// This indicates that the users password is incorrect, although the
    /// username is correct.
    IncorrectPassword,
}

impl fmt::Display for LoginError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::IncorrectUsername => write!(f, "INCORRECT_USERNAME"),
            Self::IncorrectPassword => write!(f, "INCORRECT_PASSWORD"),
        }
    }
}

impl Error for LoginError {}
