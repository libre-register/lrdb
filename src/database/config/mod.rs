use serde::{Deserialize, Serialize};
use std::time::Duration;

/// This struct stores the configuration of a database. This is not
/// included in the actual database as it is a configuration rather than
/// a peice of data.
#[derive(Debug, Clone, Eq, PartialEq, Deserialize, Serialize)]
pub struct Config {
    /// How long should a user stayed logged in for?
    pub login_timeout: Duration,
}

impl Default for Config {
    /// This gives some sensible defaults
    fn default() -> Self {
        Self {
            // Set the login timeout to be once every hour.
            login_timeout: Duration::from_secs(60 * 60),
        }
    }
}
