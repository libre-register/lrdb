//! # Link
//!
//! This includes functions to glue everthing together.

use super::Database;
use diesel::prelude::*;
use std::error::Error;
use crate::schema::login_contacts_mapping;

/// A struct to link a contact and a login user.
#[derive(Queryable, Insertable, AsChangeset, Clone, Eq, PartialEq)]
#[table_name = "login_contacts_mapping"]
pub struct ContactsLoginLink<'a> {
    contacts_id: &'a str,
    username: &'a str,
}

impl Database {

    /// Lets you link a login user to a contact.
    pub fn link_login_to_contact(
        &self,
        user: &lrau::User,
        contact: &contack::Contact,
    ) -> Result<(), Box<dyn Error>> {
        use crate::schema::login_contacts_mapping::{table, dsl};

        // Generate the ContactsLoginLink
        let link = ContactsLoginLink {
            contacts_id: &contact.uid,
            username: &user.username,
        };

        // Insert it
        diesel::insert_into(table)
            .values(&link)
            .on_conflict((dsl::contacts_id, dsl::username))
            .do_update()
            .set(&link)
            .execute(&self.pool.get()?)?;

        Ok(())
    }
}

