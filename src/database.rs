use config::Config;
use sqlx::migrate::Migrator;
use sqlx::postgres::{PgPool, PgPoolOptions};
use std::error::Error;

mod config;
mod contacts;
mod login;
// mod link;

pub const MIGRATIONS: Migrator = {
    let mut migrator = sqlx::migrate!("./migrations");
    migrator.ignore_missing = true;
    migrator
};

/// This is a struct to store the database. It is, infact, just a wrapper
/// around a `PgConnection`.
pub struct Database {
    pub pool: PgPool,
    pub config: Config,
}

// This is for the main impls, connecting and running migrations. This does
// not include manipulating data as that is done in separate modules
impl Database {
    /// Gets a pool of `PgConnection`.
    async fn new_pool(database_url: &str) -> Result<PgPool, sqlx::Error> {
        Ok(PgPoolOptions::new()
            .max_connections(5)
            .connect(database_url)
            .await?)
    }

    /// Creates a new database
    pub async fn new(database_url: &str) -> Result<Self, Box<dyn Error>> {
        // Get the connection.
        let pool = Self::new_pool(database_url).await?;

        // Wraps it
        let db = Database {
            pool,
            config: Config::default(),
        };

        // Run migrations
        db.run_migrations().await?;

        // Returns the sucsessful db, hopefully.
        Ok(db)
    }

    /// This runs the migrations on a database, creating tables and suchlike.
    /// This is automatically called from [`new`](Self::new) and so you
    /// probably don't need to call it.
    pub async fn run_migrations(&self) -> Result<(), Box<dyn Error>> {
        // Include the migrations
        MIGRATIONS.run(&self.pool).await?;
        contack::sql::SQLX_MIGRATIONS.run(&self.pool).await?;
        Ok(())
    }
}
