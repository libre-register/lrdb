use contack::read_write::vcard::VCard;
use contack::*;
use dotenv::dotenv;
use lrdb::database::Database;
use std::convert::TryInto;
use std::env;
use std::error::Error;

#[tokio::test]
async fn contact_test() -> Result<(), Box<dyn Error>> {
    dotenv().ok();

    // Load the VCard
    let vcard = include_str!("vcard.vcf");
    let vcard: VCard = vcard.parse()?;
    let mut contact: Contact = vcard.try_into()?;

    // Create a DB
    let db = Database::new(
        &env::var("DATABASE_URL").expect("DATABASE_URL MUST BE SET"),
    )
    .await?;

    // Insert the contact
    db.insert_contact(&mut contact).await?;

    Ok(())
}
